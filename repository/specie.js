const { geSpecieModel } = require("../models/specie");
const fetch = require("node-fetch");

const createSpecie = async (
  nombre,
  clasificacion,
  designacion,
  alturaPromedio,
  colorPiel,
  colorCabello,
  colorOjo,
  promedioVida,
  idioma,
) => {
  const specie = await geSpecieModel();

  return await specie.create({
    nombre,
    clasificacion,
    designacion,
    alturaPromedio,
    colorPiel,
    colorCabello,
    colorOjo,
    promedioVida,
    idioma,
  });
};

const findAllSpecies = async () => {
  const specie = await geSpecieModel();
  return await specie.findAll();
};

const findSpecieById = async (id) => {
  const url = `${process.env.URL_SWAPI_SPECIE_ID}/${id}`;
  const resSpecies = await fetch(url);
  const specie = await resSpecies.json();
  return specie;
};

module.exports = {
  createSpecie,
  findAllSpecies,
  findSpecieById,
};
