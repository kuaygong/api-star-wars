const { createSpecieHandler } = require("./handlers/create-specie");
const { getIdSpecieHandler } = require("./handlers/get-id-specie");
const { getSpeciesHandler } = require("./handlers/get-species");

module.exports = {
  createSpecieHandler,
  getIdSpecieHandler,
  getSpeciesHandler,
};
