# Reto Técnico

## Crear el secreto en Secret manager

```
Nombre del secreto: secret_manager_dev_star_wars
```

## Crear variables en el secreto previamente creado

```
DB_HOST: ****
DB_NAME: ****
DB_USER: ****
DB_PASSWORD: ****
DB_PORT: ****
```

## Configurar los Key's de AWS en gitlab para el despliegue
```
ACCESS_KEY
SECRET_KEY
```

## Subir los cambios a una rama

```
git push origin [branch]
```

## Crear Tabla

Ejecutar el siguiente codigo SQL para crear la tabla y guardar las especies

```
CREATE TABLE specie(
id int primary key auto_increment,
nombre varchar(100),
clasificacion varchar(100),
designacion varchar(100),
alturaPromedio varchar(100),
colorPiel varchar(100),
colorCabello varchar(100),
colorOjo varchar(100),
promedioVida varchar(100),
idioma varchar(100)
);

```
## Consultas API

### Obtener Especies

```
curl --location 'https://8f27qzb4rc.execute-api.us-east-2.amazonaws.com/dev/v1/api/get-species'
```

### Obtener Especie Por Id

```
curl --location 'https://8f27qzb4rc.execute-api.us-east-2.amazonaws.com/dev/v1/api/get-id-specie' \
--header 'Content-Type: application/json' \
--data '{
    "id": 1
}'
```

### Crear Especie

```
curl --location 'https://8f27qzb4rc.execute-api.us-east-2.amazonaws.com/dev/v1/api/create-specie' \
--header 'Content-Type: application/json' \
--data '{
    "nombre":"test",
      "clasificacion":"test",
      "designacion":"test",
      "alturaPromedio": 10,
      "colorPiel":"negro",
      "colorCabello":"marron",
      "colorOjo":"amarillo",
      "promedioVida":10,
      "idioma":"ingles"
}'
```